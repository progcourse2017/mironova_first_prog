// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
using namespace std;


int main(int argc, char**argv)
{
	unsigned int hour_1, min_1, sec_1;
	unsigned int hour_2, min_2, sec_2;
	unsigned int hour_3, min_3, sec_3;
	int d1, d2, d3;
	if (argc == 7) {
		hour_1 = atoi(argv[1]);
		min_1 = atoi(argv[2]);
		sec_1 = atoi(argv[3]);
		hour_2 = atoi(argv[4]);
		min_2 = atoi(argv[5]);
		sec_2 = atoi(argv[6]);
	}
	else{
		cout << "Enter the first time.\n" << "Enter a number from 0 to 23, because in 24 hours." << "\n" << "Hours:";
		cin >> hour_1;
		while (hour_1 > 23)
		{
			cout << "Error.Enter a number from 0 to 23, because in 24 hours" << "\n" << "Hours:";
			cin >> hour_1;
		}
		cout << "\n" << "Enter a number from 00 to 59, because in an hour 60 minutes." << "\n" << "Minutes:";
		
		cin >> min_1;
		while (min_1 > 59)
		{
			cout << "Error.Enter a number from 0 to 59, because in an hour 60 minutes." << "\n" << "Minutes:";
			cin >> min_1;
		}

		cout << "\n" << "Enter a number from 00 to 59, because in an minute 60 seconds." << "\n" << "Seconds:";

		
		cin >> sec_1;
		while (sec_1 > 59)
		{
			cout << "Error.Enter a number from 0 to 59, because in an minute 60 seconds." << "\n" << "Seconds:";
			cin >> sec_1;
		}
		cout << "Enter the second time.\n" << "Enter a number from 0 to 23, because in 24 hours." << "\n" << "Hours:";
		cin >> hour_2;
		while (hour_2 > 23)
		{
			cout << "\n" << "Error.Enter a number from 0 to 23, because in 24 hours" << "\n" << "Hours:";
			cin >> hour_2;
		}
		cout << "\n" << "Enter a number from 00 to 59, because in an hour 60 minutes." << "\n" << "Minutes:";
		cin >> min_2;
		while (min_2 > 59)
		{
			cout << "Error.Enter a number from 0 to 59, because in an hour 60 minutes." << "\n" << "Minutes:";
			cin >> min_2;
		}

		cout << "\n" << "Enter a number from 00 to 59, because in an minute 60 seconds." << "\n" << "Seconds:";
		
		cin >> sec_2;
		while (sec_2 > 59)
		{
			cout << "Error.Enter a number from 0 to 59, because in an minute 60 seconds." << "\n" << "Seconds:";
			cin >> sec_2;
		}
		cout << "\n" << "The first time: " << hour_1 << " hours" << min_1 << " minutes" << sec_1 << " seconds" << "\n";
		cout << "\n" << "The second time: " << hour_2 << " hours" << min_2 << " minutes" << sec_2 << " seconds" << "\n";
		d1 = hour_2 - hour_1;
		hour_3 = abs(d1);
		d2 = min_2 - min_1;
		min_3 = abs(d2);
		d3 = sec_2 - sec_1;
		sec_3 = abs(d3);
		cout << "Time difference is " << hour_3 << "hours" << min_3 << "minutes" << sec_3 << "seconds" << "\n";
		system("pause");
		return 0;

	}
	system("pause");
}


